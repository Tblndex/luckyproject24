import './App.css';
import { useState } from 'react';
import makak from './components/image/image.jpg'
import logo from './logo.svg';
import Technologies from './components/Technologies';
import Man from './components/Man';

function App() {
  return (
    <div className="App">
      <header className="App-header">

        <Man image = {makak} fio = 'Петров Дмитрий Юрьевич'/>
        <Man image = {makak} fio = 'Андреев Сергей Витальевич'/>
        <Man image = {makak} fio = 'Венедиктов Михаил Сергеевич'/>
        <Man image = {makak} fio = 'Пуртов Сергей Генадъевич'/>

        <Technologies title = 'Моя компетенция 1' description='Принятие решений.' level = '99+'/>
        <Technologies title = 'Моя компетенция 2' description='Ответственность' level = '99+'/>
        <Technologies title = 'Моя компетенция 3' description='Ориентация на результат' level = '99+'/>
        <Technologies title = 'Моя компетенция 4' description='Решение проблем' level = '99+'/>
    
        <Technologies title = 'Моя технология компетенции которую я бы хотел изучить 1' />
        <Technologies title = 'Моя технология компетенции которую я бы хотел изучить 2' />
        <Technologies title = 'Моя технология компетенции которую я бы хотел изучить 3'/>
        <Technologies title = 'Моя технология компетенции которую я бы хотел изучить 4' />
        <Technologies title = 'Моя технология компетенции которую я бы хотел изучить 5' />
        <Technologies title = 'Моя технология компетенции которую я бы хотел изучить 6' />
        <Technologies title = 'Моя технология компетенции которую я бы хотел изучить 7' />
        <Technologies title = 'Моя технология компетенции которую я бы хотел изучить 8' />
        <Technologies title = 'Моя технология компетенции которую я бы хотел изучить 9' />
        <Technologies title = 'Моя технология компетенции которую я бы хотел изучить 10' />
     </header>
    </div>
  );
}

export default App;
