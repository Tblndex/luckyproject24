import React from 'react';
const Man = (props) => {
  return (
    <div className='card'>
      <img src={props.image} className='ava' />
      <p>{props.fio}</p>
    </div>
  );
};

export default Man;