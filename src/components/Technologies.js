import React from 'react';

const Technology = ({ title, description, level }) => {
  return (
    <div className="technology-card">
      <h3>{title}</h3>
      <p>{description}</p>
      <p>{level}</p>
    </div>
  );
};

export default Technology;